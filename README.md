# Packaging Repo Setup Docs

Sources for a responsive and dynamic single-page client-side repo setup docs
page at [pkg.labs.nic.cz/doc](https://pkg.labs.nic.cz/doc/).

It's using minimal tech:

1) [python-jinja](https://palletsprojects.com/p/jinja/) to render HTML from data
2) [alpine.js](https://alpinejs.dev/) for dynamic client-side UI with minimum JS
3) [pico.css](https://picocss.com/) for minimal semantic CSS


## Usage

```
$ ./render_pkg_doc.py [RESULT_DIR]

Rendering docs into result dir: out
Rendering index template: index.html.j2 -> index.html
Converting repo data: repos.yaml -> repos.json
Copying static dirs: css, js, icons, scripts
Copying favicon: favicon.ico
Result index: out/index.html
```

The result in `out/` dir is static and can be served as files by any web server.


## Data

All repo data are stored in a separate data file [repos.yaml](repos.yaml).

YAML is used for the source data file because it's fairly easy to read and
modify with nice diffs in VCS.

This is rendered into `repos.json` and fetched using JS on page load.

`repos.yaml` can be generated using
[arepo](https://gitlab.nic.cz/packaging/arepo), see
[import_arepo.sh](import_arepo.sh) script.


## Development

You can modify:

- logic: [render_pkg_doc.py](render_pkg_doc.py)
- HTML: [index.html.j2](index.html.j2)
- CSS: [css/style.css](css/style.css)

When developing, I suggest running a local webserver in `out/` dir:

```
$ ./render_pkg_doc.py
$ cd out
$ python3 -m http.server
```

To apply your changes, simply re-run `render_pkg_doc.py` and reload browser.

This could be further automated using `inotify` or similar.

#!/usr/bin/env python3
"""
Render pkg.labs.nic.cz repo setup docs HTML page
"""
import argparse
from pathlib import Path
from shutil import copy, copytree, rmtree
import json
import yaml

import jinja2


INDEX_TEMPLATE = 'index.html.j2'
INDEX_OUTPUT = 'index.html'
DATA_INPUT = 'repos.yaml'
DATA_OUTPUT = 'repos.json'
RESULT_DIR = 'out'
STATIC_DIRS = ['css', 'js', 'icons', 'scripts']
FAVICON = 'favicon.ico'


def render_index(result_dir):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
    template = env.get_template(INDEX_TEMPLATE)
    data = yaml.safe_load(open(DATA_INPUT, 'r'))
    output = template.render(**data)
    out_index = result_dir / INDEX_OUTPUT
    with out_index.open('w') as f:
        f.write(output)
    return out_index


def render_data(result_dir):
    out_path = result_dir / DATA_OUTPUT
    data = yaml.safe_load(open(DATA_INPUT, 'r'))
    json.dump(data, out_path.open('w'))


def copy_static_files(result_dir):
    for indir in STATIC_DIRS:
        outdir = result_dir / indir
        if outdir.exists():
            print("Removing existing static dir: %s"  % outdir)
            rmtree(outdir)
        copytree(indir, outdir)


def render_doc(result_dir):
    print("Rendering docs into result dir: %s" % result_dir)
    result_dir.mkdir(parents=True, exist_ok=True)
    print("Rendering index template: %s -> %s" % (INDEX_TEMPLATE, INDEX_OUTPUT))
    out_index = render_index(result_dir)
    print("Converting repo data: %s -> %s" % (DATA_INPUT, DATA_OUTPUT))
    render_data(result_dir)
    print("Copying static dirs: %s" % ", ".join(STATIC_DIRS))
    copy_static_files(result_dir)
    print("Copying favicon: %s" % FAVICON)
    copy(FAVICON, result_dir / FAVICON)
    print("Result index: %s" % out_index)


def cli():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "result_dir", type=Path, nargs='?', default=Path(RESULT_DIR),
        help="output results into specified dir")
    args = parser.parse_args()
    render_doc(args.result_dir)


if __name__ == "__main__":
    cli()

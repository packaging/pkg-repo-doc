#!/usr/bin/env bash

SOURCE_DIR=out/
PKGDOC_DIR=/srv/aptly/.aptly/public/doc

rsync -rvu --delete "$SOURCE_DIR" "$PKGDOC_DIR"
